﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatTest.Model
{
  public  class MessageModel
    {
        public string MessageId { get; set; }
        public string Text { get; set; }
        public string DialogId { get; set; }
        public long DateSent { get; set; }

        public int RecepientId { get; set; }
        public int SenderId { get; set; }
        public bool IsRead { get; set; }

        public string RecepientFullName { get; set; }
    }
}
