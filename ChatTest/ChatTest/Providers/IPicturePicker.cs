﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatTest.Providers
{
    public interface IPicturePicker
    {
        Task<Stream> GetImageStreamAsync();
        void SavePictureToDisk(string filename, byte[] imageData);

        Task<Stream> GetVideoStreamAsync();
        void SaveVideoToDisk(string filename, byte[] videoData);

    }
}
