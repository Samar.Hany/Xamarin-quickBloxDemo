﻿using ChatTest.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ChatTest.CustomCells
{
    public partial class UserCustomCell : ViewCell
    {
        public UserCustomCell()
        {
            InitializeComponent();
            
        }
        void OnCheckBoxChanged(object sender, bool isChecked) {
            if (isChecked) {
              string userId=  ((CheckBox)sender).Text;
                App.QbProvider.UsersToChatWith.Add(new UserModel { Id =Int32.Parse(userId), Name=Label.Text  });
                    }
            else {
                int lastIndex = App.QbProvider.UsersToChatWith.Count - 1;
                App.QbProvider.UsersToChatWith.RemoveAt(lastIndex);
            }
        }


    }
}
