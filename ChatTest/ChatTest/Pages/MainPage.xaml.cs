﻿using ChatTest.Pages;
using Quickblox.Sdk;
using Quickblox.Sdk.Modules.UsersModule.Requests;
using Quickblox.Sdk.Modules.UsersModule.Responses;
using System;
using System.Collections.Generic;
using System.Net;
using Xamarin.Forms;


namespace ChatTest.Pages
{
    public partial class MainPage : ContentPage
    {



        public MainPage()
        {
            InitializeComponent();

            this.Name.Text = "samar.hany@lavaloon.com";
            this.Password.Text = "12345678";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
         
            SignInButton.Clicked += OnClicked;
            SignUpButton.Clicked += OnSignUpClicked;
        }


        private async void OnSignUpClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.IsEnabled = false;

            busyIndicator.IsVisible = true;
            var loginValue = Name.Text.Trim();
            var passwordValue = Password.Text.Trim();
            if (!string.IsNullOrEmpty(loginValue) && !string.IsNullOrEmpty(passwordValue))
            {
                var response = await App.QbProvider.SignUp(loginValue, passwordValue);
               busyIndicator.IsVisible = false;
               

                if (response.StatusCode .Equals(HttpStatusCode.Created))
                {
                    await DisplayAlert("Sign up", "Sign up sucessfully", "Ok");
                    button.IsEnabled = true;
                }
                else
                {
                    foreach(var x in response.Errors)
                    {
                        await DisplayAlert("Error", " " + x.Value[0], "Ok");
                        
                    }

                    button.IsEnabled = true;

                }
            }

        }
        private async void OnClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.IsEnabled = false;

            busyIndicator.IsVisible = true;
            var loginValue = Name.Text.Trim();
            var passwordValue = Password.Text.Trim();
            if (!string.IsNullOrEmpty(loginValue) && !string.IsNullOrEmpty(passwordValue))
            {
                var userId = await App.QbProvider.LoginWithEmailAsync(loginValue, passwordValue);
                busyIndicator.IsVisible = false;
              
                if (userId > 0)
                {
                     await Navigation.PushModalAsync(new DialogsListPage(new UserModel { Id = userId, Login = loginValue, Password = passwordValue, Name = loginValue }));
                    //await Navigation.PushModalAsync(new Page1());
                   button.IsEnabled = true;
                }
                else
                {
                    await DisplayAlert("Error","Please check your Email and password", "Ok");
                    button.IsEnabled = true;
                }
            }
        }
    }
}
