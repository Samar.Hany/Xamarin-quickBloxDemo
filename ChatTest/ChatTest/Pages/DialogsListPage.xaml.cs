﻿using ChatTest.ViewModels;
using Quickblox.Sdk.Modules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatTest.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DialogsListPage : ContentPage
    {
        UserModel user;
        DialogViewModel vm;
        public DialogsListPage(UserModel user)
        {
            InitializeComponent();
            this.user = user;
             vm = new DialogViewModel();
            this.BindingContext = vm;
           
        }


        protected override void OnAppearing()
        {
            base.OnAppearing();
          // var vm = this.BindingContext as DialogViewModel;
            vm.OnAppearingAsync();


        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            vm.Dialogs = null;
        }
        private async void listViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Dialog selectedDialog = (Dialog)e.SelectedItem;
            await App.Navigation.PushModalAsync(new ChatListViewPage(selectedDialog));

        }
        private async void CreateChatClicked(object sender, System.EventArgs e)
        {
             await Navigation.PushModalAsync(new UsersListPage(user));
            
        }
    }
}
