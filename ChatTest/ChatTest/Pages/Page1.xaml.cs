﻿using Quickblox.Sdk.Modules.AuthModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Quickblox.Sdk.Modules.UsersModule.Models;
using Quickblox.Sdk.Modules.ChatXmppModule;
using Xmpp.Im;

namespace ChatTest.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
       

        public Page1()
        {
            InitializeComponent();

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
           var userList= await App.QbProvider.GetAllUsers();

         Quickblox.Sdk.Modules.UsersModule.Models.User user= userList.First(u => u.Id != App.QbProvider.UserId);
            
            App.QbProvider.UsersToChatWith = new List<UserModel>();
            App.QbProvider.UsersToChatWith.Add(new UserModel { Id = user.Id, Login = user.Login, Name = user.Login });
          var dialog= await App.QbProvider.createChat();

            var opponentId = dialog.OccupantsIds.First(id => id != App.QbProvider.UserId);
            App.QbProvider.UsersToChatWith = new List<UserModel>();
            App.QbProvider.UsersToChatWith.Add(new UserModel { Id = opponentId });

            PrivateChatManager privateChatManager = App.QbProvider.GetXmppClient().GetPrivateChatManager(opponentId, dialog.Id);
            privateChatManager.MessageReceived += OnMessageReceived;


        }

        private  void OnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            string decodedMessage = "null";
            if (messageEventArgs.MessageType == MessageType.Chat ||
                messageEventArgs.MessageType == MessageType.Groupchat)
            {
                 decodedMessage = System.Net.WebUtility.UrlDecode(messageEventArgs.Message.MessageText);
            }
            DisplayAlert("message", decodedMessage, "cancel");
        }
        }
}
