﻿using ChatTest.Model;
using ChatTest.Providers;
using ChatTest.ViewModels;
using Quickblox.Sdk.GeneralDataModel.Models;
using Quickblox.Sdk.Modules.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatTest.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatListViewPage : ContentPage
    {
        Dialog dialog;
        public ObservableCollection<MessageModel> Messages;
        List<UserModel> users;
        public static Image ImageAttachment;
        public ChatListViewPage(Dialog dialog)
        {
            InitializeComponent();
          //  Title = "Monkey Chat";
            this.dialog = dialog;
            this.users = App.QbProvider.Users;
           
        }
    
        protected override async void OnAppearing()
        {

            ChatListViewModel chatViewModel = new ChatListViewModel(dialog,users);
            this.BindingContext = chatViewModel;
            chatViewModel.OnAppearingAsync();
         

        }

        

        private async void MyListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //  MessagesListView.SelectedItem = null;
            
            MessageViewModel message = (MessageViewModel)e.SelectedItem;
            foreach (Attachment attachment in message.AttachmentsList)
            {
                var imageAsBytes = await App.QbProvider.downloadImageAsync(attachment.Id);
                // setImageAttachment(ImageSource.FromStream(() => new MemoryStream(imageAsBytes)));
                DependencyService.Get<IPicturePicker>().SaveVideoToDisk("video",imageAsBytes);
                //file
                // string s = Encoding.UTF8.GetString(imageAsBytes,0,imageAsBytes.Length);
                //await DisplayAlert("", s, "ok");

            
            }
        }

        private async void MyListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            // MessagesListView.SelectedItem = null;
            
          

        }

        public async void ScrollList()
        {
            var sorted = MessagesListView.ItemsSource as ObservableCollection<MessageViewModel>;
            try
            {
                if (sorted != null && sorted.Count > 1)
                {
                    await Task.Delay(500);
                    MessagesListView.ScrollTo(sorted[sorted.Count - 1], ScrollToPosition.End, false);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
