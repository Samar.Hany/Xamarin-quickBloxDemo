﻿using ChatTest.Model;
using ChatTest.ViewModels;
using Quickblox.Sdk.GeneralDataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;
using Quickblox.Sdk.Modules.Models;

namespace ChatTest.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PrivateChatPage : ContentPage
    {
        Dialog dialog;

        public ObservableCollection<MessageModel> Messages;

        public PrivateChatPage(Dialog dialog)
        {
            InitializeComponent();
            this.dialog = dialog;
        }

        protected override void OnAppearing()
        {
            BaseChatViewModel baseChatViewModel = new BaseChatViewModel(dialog.Id);
            this.BindingContext = baseChatViewModel;
           Messages= baseChatViewModel.Messages;

            baseChatViewModel.OnAppearingAsync();
        }

        public async void ScrollList()
        {
            var sorted = listView.ItemsSource as ObservableCollection<MessageModel>;
            try
            {
                if (sorted != null && sorted.Count > 1)
                {
                    await Task.Delay(500);
                    listView.ScrollTo(sorted[sorted.Count - 1], ScrollToPosition.End, false);
                }
            }
            catch (Exception ex)
            {
            }
        }


    }
}
