﻿
using ChatTest.Pages;
using ChatTest.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ChatTest.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UsersListPage : ContentPage
    {
      public  List<UserModel> usersList=new List<UserModel>();
        UserModel currentUser;
        List<UserModel> users;
        public UsersListPage(UserModel currentUser)
        {
            InitializeComponent();
            this.currentUser = currentUser;
            UsersViewModel vm = new UsersViewModel();
            this.BindingContext = vm;
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            var vm = this.BindingContext as UsersViewModel;
            App.QbProvider.UsersToChatWith = new List<UserModel>();
            vm.OnAppearingAsync();
            
           
        }

       

        private void ListViewOnItemTapped(object sender, ItemTappedEventArgs e)
        {
            listView.SelectedItem = null;

        }
        private async void listViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            listView.SelectedItem = null;

        }

        private async void CreateChatClicked(object sender, System.EventArgs e)
        {
           
            users = (List<UserModel>)listView.ItemsSource;
            App.QbProvider.Users = users;
            await App.Navigation.PushModalAsync(new ChatListViewPage(null));
           
        }
    }

}
