﻿using ChatTest.Model;
using ChatTest.Pages;
using Quickblox.Sdk.GeneralDataModel.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChatTest.ViewModels
{
  public class MessageViewModel : BaseViewModel
    {
        private string text;

        private List<Attachment> attachmentsList;

        public string Text
        {
            get { return text; }
            set { text = value; RaisePropertyChanged(); }
        }

        private DateTime messageDateTime;

        public DateTime MessagDateTime
        {
            get { return messageDateTime; }
            set { messageDateTime = value; RaisePropertyChanged(); }
        }

        private bool isIncoming;

        public bool IsIncoming
        {
            get { return isIncoming; }
            set { isIncoming = value; RaisePropertyChanged(); }
        }

        public bool HasAttachement => !string.IsNullOrEmpty(attachementUrl);

        private string attachementUrl;

        public string AttachementUrl
        {
            get { return attachementUrl; }
            set { attachementUrl = value; RaisePropertyChanged(); }
        }

        public string reciepientName;

    public string ReciepientName
        {
            get
            {
                return reciepientName;
            }

            set
            {
                reciepientName = value;
                RaisePropertyChanged();
            }
        }

        public List<Attachment> AttachmentsList
        {
            get
            {
                return attachmentsList;
            }

            set
            {
                attachmentsList = value;
                RaisePropertyChanged();
            }

        }
    }
}