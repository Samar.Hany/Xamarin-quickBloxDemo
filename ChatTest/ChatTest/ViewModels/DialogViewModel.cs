﻿using Quickblox.Sdk.Modules.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChatTest.ViewModels
{
    class DialogViewModel :ViewModel
    {
        string name;
        private static List<Dialog> dialogs;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                this.RaisePropertyChanged();
            }
        }

        public List<Dialog> Dialogs
        {
            get { return dialogs; }
            set
            {
                dialogs = value;
                this.RaisePropertyChanged();
            }
        }
        public override void OnAppearingAsync()
        {
            base.OnAppearingAsync();
            this.IsBusyIndicatorVisible = true;

            Task.Factory.StartNew(async () => {
                var list = new List<Dialog>();
                var users = new List<UserModel>();
                var baseSesionResult = await App.QbProvider.GetBaseSession();
                if (baseSesionResult)
                {
                    var dialgs = await App.QbProvider.getAllDialogs();
                    foreach (var dialog in dialgs)
                    {
                            list.Add(dialog);
                       
                    }

                    var usrs = await App.QbProvider.GetAllUsers();
                    foreach (var user in usrs)
                    {
                        if (user.Id != App.QbProvider.UserId)
                        {
                            users.Add(new UserModel() { Id = user.Id, Name = user.FullName, Login = user.Login, Password = user.Login });
                        }
                    }

                    App.QbProvider.Users = users;
                
            }

                Device.BeginInvokeOnMainThread(() => {
                    Dialogs = list;
                    this.IsBusyIndicatorVisible = false;
                });
            });
        }
    }
}
