﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChatTest.ViewModels
{
   public class UsersViewModel : ViewModel
    {
        private string login;
        private string password;
        private static List<UserModel> users;

        public UsersViewModel()
        {
           
        }

        public string Login
        {
            get { return login; }
            set
            {
                login = value;
                this.RaisePropertyChanged();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                this.RaisePropertyChanged();
            }
        }

        public List<UserModel> Users
        {
            get { return users; }
            set
            {
                users = value;
                this.RaisePropertyChanged();
            }
        }

      

        public override void OnAppearingAsync()
        {
            base.OnAppearingAsync();
            this.IsBusyIndicatorVisible = true;

            Task.Factory.StartNew(async () => {
                var list = new List<UserModel>();
                var baseSesionResult = await App.QbProvider.GetBaseSession();
                if (baseSesionResult)
                {
                    var users = await App.QbProvider.GetAllUsers();
                    foreach (var user in users)
                    {
                        if (user.Id != App.QbProvider.UserId)
                        {
                            list.Add(new UserModel() { Id = user.Id, Name = user.FullName, Login = user.Login, Password = user.Login });
                        }
                        }
                }

                Device.BeginInvokeOnMainThread(() => {
                    Users = list;
                    this.IsBusyIndicatorVisible = false;
                });
            });
        }




   
    }
}