﻿using ChatTest.Model;
using ChatTest.Pages;
using Quickblox.Sdk.GeneralDataModel.Models;
using Quickblox.Sdk.Modules.UsersModule.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChatTest.ViewModels
{
  public  class BaseChatViewModel : ViewModel

    {
        private string dialogName;
        private string messageText;

        protected List<User> opponentUsers = new List<User>();
        protected string dialogId;


        public ObservableCollection<MessageModel> Messages { get; private set; }

        public BaseChatViewModel(string dialogId)
        {
            this.dialogId = dialogId;
           this.Messages = new ObservableCollection<MessageModel>();
        }

        public string DialogName
        {
            get { return dialogName; }
            set
            {
                dialogName = value;
                this.RaisePropertyChanged();
            }
        }


        public string MessageText
        {
            get { return messageText; }
            set
            {
                messageText = value;
                this.RaisePropertyChanged();
            }
        }

        public async Task LoadMessages(string dialogId)
        {
            List<Message> messages;
            try
            {
                messages = await App.QbProvider.GetMessagesAsync(dialogId);
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.ToString(), "Ok");
                return;
            }

            if (messages != null)
            {
                messages = messages.OrderBy(message => message.DateSent).ToList();
                foreach (var message in messages)
                {
                    var chatMessage = new MessageModel();
                    chatMessage.DateSent = message.DateSent;
                    chatMessage.SenderId = message.SenderId;
                    chatMessage.MessageId = message.Id;
                    if (message.RecipientId.HasValue)
                        chatMessage.RecepientId = message.RecipientId.Value;
                    chatMessage.DialogId = message.ChatDialogId;
                    chatMessage.IsRead = message.Read == 1;

                    // await this.SetRecepientName(chatMessage);

                    chatMessage.Text = System.Net.WebUtility.UrlDecode(message.MessageText);

                    Device.BeginInvokeOnMainThread(() =>
                        Messages.Add(chatMessage)
                   );
                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    var privateChatPage = ((App.Current.MainPage as NavigationPage).CurrentPage as PrivateChatPage);
                    if (privateChatPage != null)
                    privateChatPage.ScrollList();
                }
                );
            }

        }

        public override async void OnAppearingAsync()
        {
            await LoadMessages(dialogId);
        }
    }

}
