﻿using ChatTest.Model;
using ChatTest.Pages;
using ChatTest.Providers;
using Quickblox.Sdk.GeneralDataModel.Models;
using Quickblox.Sdk.Modules.ChatXmppModule;
using Quickblox.Sdk.Modules.ChatXmppModule.ExtraParameters;
using Quickblox.Sdk.Modules.ChatXmppModule.Models;
using Quickblox.Sdk.Modules.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xmpp.Im;

namespace ChatTest.ViewModels
{
   public class ChatListViewModel : ViewModel
    {

        private ObservableCollection<MessageViewModel> messagesList;

    public ObservableCollection<MessageViewModel> Messages
    {
        get { return messagesList; }
        set { messagesList = value; RaisePropertyChanged(); }
    }

    private string outgoingText;

    public string OutGoingText
    {
        get { return outgoingText; }
        set { outgoingText = value; RaisePropertyChanged(); }
    }
        Stream stream;
    public ICommand SendCommand { get; set; }
        public ICommand AttachCommand { get; set; }
        Dialog dialog;
        private GroupChatManager groupChatManager;
        private PrivateChatManager privateChatManager;
        List<UserModel> users;
        public ChatListViewModel(Dialog dialog, List<UserModel> users)
        {
            // Initialize with default values

            Messages = new ObservableCollection<MessageViewModel>();
            this.dialog = dialog;
            this.users = users;
           
        
            OutGoingText = null;
        SendCommand = new Command( () =>
        {
            
   
            var message = OutGoingText;
            if (!string.IsNullOrEmpty(message))
            {
                long unixTimestamp = DateTime.UtcNow.Ticks - new DateTime(1970, 1, 1).Ticks;
                unixTimestamp /= TimeSpan.TicksPerSecond;
                var m = new MessageViewModel
                {
                    Text = OutGoingText,
                    IsIncoming = false,
                    MessagDateTime = UnixTimeStampToDateTime(unixTimestamp)
                };
                this.Messages.Add(m);
                try
                {
                    var encodedMessage = System.Net.WebUtility.UrlEncode(message);

                    if (App.QbProvider.UsersToChatWith.Count == 1)
                    {
                        privateChatManager.SendMessage(encodedMessage);
                        
                    }
                    else
                    {
                        groupChatManager.SendMessage(encodedMessage);
                    }
                }
                catch (Exception ex)
                {
                    return;
                    //await App.Current.MainPage.DisplayAlert ("Error", ex.ToString (), "Ok");
                }

            }

            OutGoingText = null;

            Device.BeginInvokeOnMainThread(() =>
        {
            var page = (App.Current.MainPage as NavigationPage).CurrentPage as ChatListViewPage;
            if (page != null)
                page.ScrollList();

        });
        });


            AttachCommand = new Command(async () => {
                //stream = await DependencyService.Get<IPicturePicker>().GetImageStreamAsync();
                stream = await DependencyService.Get<IPicturePicker>().GetVideoStreamAsync();

                //file
                // var assembly = typeof(ChatListViewModel).GetTypeInfo().Assembly;
                //Stream stream = assembly.GetManifestResourceStream("ChatTest.TextFile1.txt");

                var attachmentId = await App.QbProvider.UploadPrivateVideoAsync(stream);
                // AttachmentExtraParamValue attachment = new AttachmentExtraParamValue();
                //   attachment.Id = attachmentId.ToString();

                // ChatMessageExtraParameter chatMessage = new ChatMessageExtraParameter(dialog.Id,true,attachment);

                AttachmentTag attachmentTag = new AttachmentTag();
                attachmentTag.Id = attachmentId.ToString();
                privateChatManager.SendAttachemnt(attachmentTag);


                long unixTimestamp = DateTime.UtcNow.Ticks - new DateTime(1970, 1, 1).Ticks;
                unixTimestamp /= TimeSpan.TicksPerSecond;
                var m = new MessageViewModel
                {
                    Text = attachmentTag.Id,
                    IsIncoming = false,
                    MessagDateTime = UnixTimeStampToDateTime(unixTimestamp)
                };

                m.AttachmentsList = new List<Attachment>();
                m.AttachmentsList.Add(new Attachment {Id= attachmentTag.Id });
                this.Messages.Add(m);
                
                Device.BeginInvokeOnMainThread(() =>
                {
                    var page = (App.Current.MainPage as NavigationPage).CurrentPage as ChatListViewPage;
                    if (page != null)
                        page.ScrollList();

                });
            });
        }

        


        public async Task LoadMessages(string dialogId)
        {
            List<Message> messages;
            try
            {
                messages = await App.QbProvider.GetMessagesAsync(dialogId);
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.ToString(), "Ok");
                return;
            }

            if (messages != null)
            {
                messages = messages.OrderBy(message => message.DateSent).ToList();
                foreach (var message in messages)
                {
                    
        
                    var chatMessage = new MessageViewModel();
                    chatMessage.MessagDateTime = UnixTimeStampToDateTime(message.DateSent);

                    if (App.QbProvider.UserId == message.SenderId)
                    {
                        chatMessage.IsIncoming = false;
                      
                    }
                    else
                    {
                        chatMessage.IsIncoming = true;
                        await this.SetRecepientName(message.SenderId, chatMessage);
                    }

                    // chatMessage.SenderId = message.SenderId;
                    // chatMessage.MessageId = message.Id;
                    // if (message.RecipientId.HasValue)
                    //  chatMessage.RecepientId = message.RecipientId.Value;
                    // chatMessage.DialogId = message.ChatDialogId;
                    //chatMessage.IsRead = message.Read == 1;

                    chatMessage.AttachmentsList = new List<Attachment>();
                    foreach(Attachment attachment in message.Attachments)
                    {
                        chatMessage.AttachmentsList.Add(attachment);
                    }

                    chatMessage.Text = System.Net.WebUtility.UrlDecode(message.MessageText);

                    Device.BeginInvokeOnMainThread(() =>
                        Messages.Add(chatMessage)
                   );
                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    var listViewPage = ((App.Current.MainPage as NavigationPage).CurrentPage as ChatListViewPage);
                   if (listViewPage != null)
                        listViewPage.ScrollList();
                }
                );
                
            }

        }

        protected async Task SetRecepientName(int senderId,MessageViewModel message)
        {
           foreach(UserModel user in users){
                if (user.Id == senderId)
                {
                    message.ReciepientName = user.Name.Substring(0,3);
                    return;
                }

            }
               
                  
            
        }

        private async void OnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            //if (messageEventArgs.Message.MessageText == null) return;
            

            
            if (messageEventArgs.MessageType == MessageType.Chat ||
                messageEventArgs.MessageType == MessageType.Groupchat)
            {
                string decodedMessage = System.Net.WebUtility.UrlDecode(messageEventArgs.Message.MessageText);

                var messageViewModel = new MessageViewModel();

                messageViewModel.MessagDateTime=UnixTimeStampToDateTime( messageEventArgs.Message.DateSent);

                if (messageEventArgs.Message.NotificationType != 0)
                {
                    if (messageEventArgs.Message.NotificationType == NotificationTypes.GroupUpdate)
                    {
                        if (messageEventArgs.Message.AddedOccupantsIds.Any())
                        {
                            var userIds = new List<int>(messageEventArgs.Message.AddedOccupantsIds);
                            userIds.Add(messageEventArgs.Message.SenderId);
                            var users = await App.QbProvider.GetUsersByIdsAsync(string.Join(",", userIds));
                            var addedUsers = users.Where(u => u.Id != messageEventArgs.Message.SenderId);
                            var senderUser = users.First(u => u.Id == messageEventArgs.Message.SenderId);
                            messageViewModel.Text = senderUser.FullName + " added users: " +
                                                string.Join(",", addedUsers.Select(u => u.FullName));
                        }
                        else if (messageEventArgs.Message.DeletedOccupantsIds.Any())
                        {
                            var userIds = new List<int>(messageEventArgs.Message.DeletedOccupantsIds);
                            var users = await App.QbProvider.GetUsersByIdsAsync(string.Join(",", userIds));
                                messageViewModel.Text = string.Join(",", users.Select(u => u.FullName)) + " left this room";
                        }

                      
                    }
                }
                else
                {
                    messageViewModel.Text = decodedMessage;
                }

                await SetRecepientName((int)App.QbProvider.UserId ,messageViewModel);

                this.Messages.Add(messageViewModel);

                Device.BeginInvokeOnMainThread(() =>
                {
                    var page = (App.Current.MainPage as NavigationPage).CurrentPage as PrivateChatPage;
                    if (page != null)
                        page.ScrollList();
                });
            }
        }

        public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }



         public override void OnAppearingAsync()
        {
            base.OnAppearingAsync();
            this.IsBusyIndicatorVisible = true;

            Task.Factory.StartNew(async () => {

                try
                {
                    // uses login as password because it is the same
                    MessageProvider.Instance.ConnetToXmpp(App.QbProvider.currentUser.Id, App.QbProvider.currentUser.Login);
                }
                catch (Exception ex)
                {
                }

                if (dialog == null)
                {
                   dialog=await App.QbProvider.createChat();
                }
                
                    if (dialog.OccupantsIds.Count == 2)
                    {
                        // App.QbProvider.privateChat(dialog);

                        var opponentId = dialog.OccupantsIds.First(id => id != App.QbProvider.UserId);
                        App.QbProvider.UsersToChatWith = new List<UserModel>();
                        App.QbProvider.UsersToChatWith.Add(new UserModel { Id = opponentId });

                        privateChatManager = App.QbProvider.GetXmppClient().GetPrivateChatManager(opponentId, dialog.Id);
                        privateChatManager.MessageReceived += OnMessageReceived;
                    }
                    else
                    {
                    App.QbProvider.GetXmppClient().JoinToGroup(dialog.XmppRoomJid, App.QbProvider.UserId.ToString());
                    List<int> others = new List<int>();
                        App.QbProvider.UsersToChatWith = new List<UserModel>();

                        foreach (int userId in dialog.OccupantsIds)
                        {
                            if (userId != App.QbProvider.UserId)
                            {
                                others.Add(userId);
                                App.QbProvider.UsersToChatWith.Add(new UserModel { Id = userId });
                            }
                        }
                        var userIds = App.QbProvider.UsersToChatWith.Select(u => u.Id).ToList();
                        var userIdsString = string.Join(",", userIds);
                        groupChatManager = App.QbProvider.GetXmppClient().GetGroupChatManager(dialog.XmppRoomJid, dialog.Id);
                    groupChatManager.JoinGroup(App.QbProvider.UserId.ToString());
                    groupChatManager.MessageReceived += OnMessageReceived;
                      //  groupChatManager.JoinGroup(App.QbProvider.UserId.ToString());
                        //await Task.Delay(1000);
                       // groupChatManager.NotifyAboutGroupCreation(others, dialog);

                }

                App.QbProvider.GetXmppClient().MessageReceived += OnMessageReceived;
                await LoadMessages(dialog.Id);
               

                Device.BeginInvokeOnMainThread(() => {
                   
                    this.IsBusyIndicatorVisible = false;
                });
            });
           
        }

      
    }


}
