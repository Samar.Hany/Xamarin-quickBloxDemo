﻿using ChatTest.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ChatTest
{
    public partial class App : Application
    {
        public static QbProvider QbProvider { get; set; }
        public static INavigation Navigation { get; set; }

        public static bool IsInternetAvaliable { get; set; }
        private static bool isInternetMessageShowing;

        public App()
        {
            InitializeComponent();
            QbProvider = new QbProvider(ShowInternetNotification);
            var page = new MainPage();
            Navigation = page.Navigation;
            MainPage = new NavigationPage(page);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static void ShowInternetNotification()
        {
            Device.BeginInvokeOnMainThread(async () => {
                if (!IsInternetAvaliable)
                    if (!isInternetMessageShowing)
                    {
                        isInternetMessageShowing = true;
                        await App.Current.MainPage.DisplayAlert("Internet connection", "Internet connection is lost. Please check it and restart the Application", "Ok");
                        isInternetMessageShowing = false;
                    }
            });
        }
    }
}
